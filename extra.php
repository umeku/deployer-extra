<?php

use Symfony\Component\Console\Input\InputOption;

/**
 * Show releases on server.
 */
task('releases', function () {
    $releases = env('releases_list');
    $current = env('release');

    foreach ($releases as $i => $release) {
        $isCurrent = $release == $current;

        write('<fg=green>' . $i . '</fg=green>: ');
        writeln($isCurrent ? ('<fg=green>' . $release . '</fg=green>') : $release);
    }
})->desc('Show available releases on a server.');


/**
 * Rollback to previous release.
 */
option('deleteCurrent', null, InputOption::VALUE_NONE, 'Delete current release.');
option('release', null, InputOption::VALUE_OPTIONAL, 'Release to rollback to.');

task('rollback', function () {
    $releases = env('releases_list');
    $current = env('release');

    $currentId = array_search($current, $releases);
    $releaseId = $currentId + 1;

    if (!is_null($release = input()->getOption('release'))) {
        $releaseId = false !== ($releaseId = array_search($release, $releases)) ? $releaseId : (int)$release;
    }

    if (isset($releases[$releaseId])) {
        $releaseDir = "{{deploy_path}}/releases/{$releases[$releaseId]}";

        // Symlink to old release.
        run("cd {{deploy_path}} && ln -nfs $releaseDir current");

        // Remove release
        if (input()->getOption('deleteCurrent')) {
            run("rm -rf {{deploy_path}}/releases/{$releases[$currentId]}");
        }

        if (isVerbose()) {
            writeln("Rollback to `{$releases[$releaseId]}` release was successful.");
        }
    } else {
        if (!is_null($release = input()->getOption('release'))) {
            writeln("<comment>Release `{$release}` does not exist.</comment>");
        } else {
            writeln("<comment>No more releases you can revert to.</comment>");
        }
    }
})->desc('Rollback to a specific release.');

/**
 * Delete a release.
 */
task('delete', function () {
    $releases = env('releases_list');
    $current = env('release');

    $currentId = array_search($current, $releases);

    if (is_null($release = input()->getOption('release'))) {
        writeln("<comment>Please provide --release number to delete.</comment>");
    } else {

        $releaseId = false !== ($releaseId = array_search($release, $releases)) ? $releaseId : (int)$release;

        if (isset($releases[$releaseId])) {
            if ($releaseId == $currentId) {
                writeln("<comment>Cannot delete current release, rollback first.</comment>");
            } else {
                run("rm -rf {{deploy_path}}/releases/{$releases[$currentId]}");

                if (isVerbose()) {
                    writeln("Release `{$releases[$releaseId]}` was deleted successfully.");
                }
            }
        } else {
            writeln("<comment>Release `{$release}` does not exist.</comment>");
        }

    }
})->desc('Delete a release on server.');

/**
 * Initializes directory structure, optionally copying shared folders from a different installation.
 */
task('init:run', function () {
    $currentPath = ask('Path to current installation? [null]', null);

    if (empty(trim($currentPath))) {
        return;
    }

    $sharedPath = "{{deploy_path}}/shared";

    foreach (get('shared_dirs') as $dir) {
        // Delete directory if exists
        run("if [ -d $(echo $sharedPath/$dir) ]; then rm -rf $sharedPath/$dir; fi");

        // Create parent dirs to shared dir
        run("mkdir -p $(dirname $sharedPath/$dir)");

        // Copy directory
        run("if [ -d $(echo $currentPath/$dir) ]; then cp -rpf $currentPath/$dir $sharedPath/$dir; fi");
    }

    foreach (get('shared_files') as $file) {
        // Remove from source
        run("if [ -f $(echo $sharedPath/$file) ]; then rm -rf $sharedPath/$file; fi");

        // Create parent dirs to shared file
        run("mkdir -p $(dirname $sharedPath/$file)");

        // Copy file
        run("if [ -f $(echo $sharedPath/$file) ]; then cp -pf $currentPath/$file $sharedPath/$file; fi");
    }
})->setPrivate();

task('init_project', [
    'deploy:prepare',
    'init:run'
])->desc('Initialize directory structure, optionally copying shared folders from a different installation.');
