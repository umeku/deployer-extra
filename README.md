# Extra tasks for Deployer

This package adds extra tasks for Deployer.

## Features
* Initialize directory structure, optionally copying shared folders from a different installation
* Show available releases on a server
* Rollback to a specific release, optionally deleting current release
* Delete a specific release

## Usage

Add `require 'vendor/umeku/deployer-extra/extra.php'` to your **deploy.php** file.

## Tasks 

### dep init_project

Initializes directory structure, optionally copying shared folders from a different installation.

### dep releases

Shows available releases on a server. Current release is highlighted.

### dep rollback

Rollbacks to a specific release. By default it will rollback to a previous available release from current release.

Option | Description
--------------------- | ---------------------
**release** | Release number or id to rollback to. Can be either an index of the release (from **dep releases**) or a name (timestamp) of the release. 
**deleteCurrent** | Delete the current release after rollback.

### dep delete

Deletes a specific release on a server.

Option | Description
--------------------- | ---------------------
**release** | Release number or id to delete. Can be either an index of the release (from **dep releases**) or a name (timestamp) of the release.
